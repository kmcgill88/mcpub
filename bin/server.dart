import 'dart:io';

import 'package:logging/logging.dart';
import 'package:mcpub/mcpub.dart';
import 'package:mcpub/src/oauth_provider_well_known.dart';

const defaultFileSystemPath = '/mcpub';

void main(List<String> args) async {
  ProcessSignal.sigint.watch().listen((signal) {
    exit(0);
  });
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
  Logger.root.onLevelChanged.listen((level) {
    print('The new log level is $level');
  });

  final env = Platform.environment;

  final filePath = env['FILE_SYSTEM_PATH'] ?? defaultFileSystemPath;
  final fileSystemPath = [
    Directory.current.path,
    filePath,
  ].join().replaceAll('//', '/');
  Logger.root.info('FILE_SYSTEM_PATH: $fileSystemPath');

  OAuthProvider oAuthProvider = OAuthProviderNone();
  PackageStore packageStore = PackageStoreFileSystem(fileSystemPath);
  MetaStore metaStore = MetaStoreFileSystem(fileSystemPath);

  final wellKnownUrl = env['WELL_KNOWN_URL'];
  if (wellKnownUrl != null && wellKnownUrl.isNotEmpty) {
    Logger.root.info(
      'WELL_KNOWN_URL "$wellKnownUrl" provided. Using OAuthProviderWellKnown.',
    );
    oAuthProvider = OAuthProviderWellKnown(wellKnownUrl: wellKnownUrl);
  }

  final server = McPubServer(
    oAuthProvider: oAuthProvider,
    packageStore: packageStore,
    metaStore: metaStore,
  );

  await server.serve(
    host: env['HOST'] ?? '0.0.0.0',
    port: int.tryParse(env['PORT'] ?? '') ?? 4040,
  );
}
