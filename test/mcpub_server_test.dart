import 'package:mcpub/mcpub.dart';
import 'package:test/test.dart';

void main() {
  test('build finalize url', () {
    final name = 'foo';
    final version = '0.0.0';
    final uriWithPort =
        Uri.parse('http://localhost:4040/api/packages/versions/new/upload');
    expect(buildUploadFinalizeUrl(uriWithPort, name, version),
        'http://localhost:4040/api/packages/foo/finalize/0.0.0');

    final noPort =
        Uri.parse('http://localhost/api/packages/versions/new/upload');
    expect(buildUploadFinalizeUrl(noPort, name, version),
        'http://localhost/api/packages/foo/finalize/0.0.0');
  });

  test('build archive url', () {
    final name = 'foo';
    final version = '0.0.0';
    final uriWithPort =
        Uri.parse('http://localhost:4040/api/packages/versions/new/upload');
    expect(buildArchiveUrl(uriWithPort, name, version),
        'http://localhost:4040/packages/foo/versions/0.0.0.tar.gz');

    final noPort =
        Uri.parse('http://localhost/api/packages/versions/new/upload');
    expect(buildArchiveUrl(noPort, name, version),
        'http://localhost/packages/foo/versions/0.0.0.tar.gz');
  });

  test('parse host', () {
    final uri = Uri.parse('https://example.oktapreview.com/oauth2/abc123');
    expect(uri.host, 'example.oktapreview.com');
  });
}
