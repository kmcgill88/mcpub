import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:jose/jose.dart';
import 'package:mcpub/src/oauth_provider_well_known.dart';
import 'package:shelf/shelf.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

String generateTestJwt({
  String iss = 'https://example.com',
  String? sub,
}) {
  final builder = JsonWebSignatureBuilder();
  builder.jsonContent = {
    'exp': Duration(hours: 4).inSeconds,
    'iss': iss,
    'sub': sub,
  };
  builder.addRecipient(
    JsonWebKey.fromJson({
      'kty': 'RSA',
      'alg': 'RS256',
      'kid': 'abc',
      'use': 'sig',
      'e': 'AQAB',
      'n': 'qXNX5OITMYbI4wws1IC-wDpYOiQ',
    }),
  );
  final jws = builder.build();
  return jws.toCompactSerialization();
}

class MockKeystore extends JsonWebKeyStore {
  String? keyUrl;

  @override
  void addKeySetUrl(Uri url) {
    keyUrl = url.toString();
  }
}

class MockClient extends http.BaseClient {
  MockClient(this.getResponse);

  final http.Response getResponse;
  String? getUrl;

  @override
  Future<http.Response> get(Uri url, {Map<String, String>? headers}) async {
    getUrl = url.toString();
    return getResponse;
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    throw UnimplementedError();
  }
}

void main() {
  group('OAuthProviderWellKnown', () {
    group('_addKeys', () {
      test('adds jwks keys on init', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );
        await Future.delayed(Duration(seconds: 1));

        expect(mockClient.getUrl, expectedWellKnownUrl);
        expect(mockKeyStore.keyUrl, expectedJwksUrl);
      });
    });

    group('getPublisherEmail', () {
      test('throws if cant decode jwt', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        final oAuthProviderWellKnown = OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );

        expect(
          () => oAuthProviderWellKnown.getPublisherEmail({'missing': 'auth'}),
          throwsA(isA<Exception>()),
        );
      });

      test('throws if jwt missing sub', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        final oAuthProviderWellKnown = OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );

        expect(
          () => oAuthProviderWellKnown.getPublisherEmail(
            {'authorization': 'bearer ${generateTestJwt(sub: null)}'},
          ),
          throwsA(isA<Exception>()),
        );
      });

      test('happy path', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        final oAuthProviderWellKnown = OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );

        final email = await oAuthProviderWellKnown.getPublisherEmail(
          {'authorization': 'bearer ${generateTestJwt(sub: "theSubject")}'},
        );
        expect(email, 'theSubject@example.com');
      });
    });

    group('shouldAllowRequest', () {
      test('401 if no jwt provided', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        final oAuthProviderWellKnown = OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );

        final req = Request('GET', Uri.parse(expectedJwksUrl));
        final response = await oAuthProviderWellKnown.shouldAllowRequest(req);

        expect(response?.statusCode, 401);
      });

      test('401 when jwt fails to verify', () async {
        const expectedJwksUrl = 'https://example.com/oauth2/abcabc/v1/keys';
        const expectedWellKnownUrl =
            'https://example.com/.well-known/openid-configuration';

        final mockKeyStore = MockKeystore();
        final mockClient = MockClient(
          http.Response(
            json.encode({'jwks_uri': expectedJwksUrl}),
            200,
          ),
        );

        final oAuthProviderWellKnown = OAuthProviderWellKnown(
          wellKnownUrl: expectedWellKnownUrl,
          keyStore: mockKeyStore,
          client: mockClient,
        );

        final req = Request(
          'GET',
          Uri.parse(expectedJwksUrl),
          headers: {
            'authorization': 'bearer ${generateTestJwt(sub: "theSubject")}',
          },
        );
        final response = await oAuthProviderWellKnown.shouldAllowRequest(req);

        expect(response?.statusCode, 401);
      });
    });
  });
}
