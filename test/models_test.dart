import 'package:mcpub/mcpub.dart';
import 'package:test/test.dart';

void main() {
  final now = DateTime.now().toUtc();
  final nowIso = now.toIso8601String();
  group('models', () {
    group('PubPackage', () {
      test('returns the latestVersion', () {
        final v0 = PubVersion(
          version: '0.0.0',
          pubspec: {},
          createdAt: nowIso,
          uploader: nowIso,
        );
        final v1 = PubVersion(
          version: '1.0.0',
          pubspec: {},
          createdAt: now.add(Duration(days: 1)).toIso8601String(),
          uploader: nowIso,
        );
        final v2 = PubVersion(
          version: '2.0.0',
          pubspec: {},
          createdAt: now.add(Duration(days: 2)).toIso8601String(),
          uploader: nowIso,
        );
        final pubPackage = PubPackage(
          name: 'foo',
          createdAt: nowIso,
          updatedAt: nowIso,
          versions: [v0, v1, v2],
          uploaders: [],
        );

        expect(pubPackage.latestVersion, v2);
      });

      test('only 1 version for latestVersion', () {
        final v0 = PubVersion(
          version: '0.0.0',
          pubspec: {},
          createdAt: nowIso,
          uploader: nowIso,
        );
        final pubPackage = PubPackage(
          name: 'foo',
          createdAt: nowIso,
          updatedAt: nowIso,
          versions: [v0],
          uploaders: [],
        );

        expect(pubPackage.latestVersion, v0);
      });

      test('NO versions for latestVersion throws', () {
        final pubPackage = PubPackage(
          name: 'foo',
          createdAt: nowIso,
          updatedAt: nowIso,
          versions: [],
          uploaders: [],
        );

        expect(() => pubPackage.latestVersion, throwsException);
      });
    });
  });
}
