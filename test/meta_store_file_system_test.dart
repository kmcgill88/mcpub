import 'dart:convert';
import 'dart:io';

import 'package:mcpub/mcpub.dart';
import 'package:test/test.dart';

const testFilePath = 'test/test-filesystem';

void main() {
  group('MetaStoreFileSystem', () {
    group('addVersion', () {
      tearDown(() async {
        await Directory(testFilePath).delete(recursive: true);
      });

      test('can add (write) a new version to disk', () async {
        final metaStore = MetaStoreFileSystem(testFilePath);

        await metaStore.addVersion(
          'foo',
          PubVersion(
            version: '0.0.0',
            pubspec: {},
            createdAt: DateTime.now().toIso8601String(),
            uploader: 'foo@bar.dev',
          ),
        );

        expect(
          File('$testFilePath/foo/meta/versions/0.0.0.json').existsSync(),
          isTrue,
        );
        expect(
          File('$testFilePath/foo/meta/package.json').existsSync(),
          isTrue,
        );
      });

      test('first upload adds uploader email', () async {
        final metaStore = MetaStoreFileSystem(testFilePath);
        await metaStore.addVersion(
          'foo',
          PubVersion(
            version: '0.0.0',
            pubspec: {},
            createdAt: DateTime.now().toIso8601String(),
            uploader: 'foo@bar.dev',
          ),
        );
        final packageJsonString =
            await File('$testFilePath/foo/meta/package.json').readAsString();
        final package = PubPackage.fromJson(json.decode(packageJsonString));

        expect(package.uploaders.length, 1);
        expect(package.uploaders.first, 'foo@bar.dev');
      });

      test('second upload adds uploader email', () async {
        final metaStore = MetaStoreFileSystem(testFilePath);
        await metaStore.addVersion(
          'foo',
          PubVersion(
            version: '0.0.0',
            pubspec: {},
            createdAt: DateTime.now().toIso8601String(),
            uploader: 'foo@bar.dev',
          ),
        );
        await metaStore.addVersion(
          'foo',
          PubVersion(
            version: '0.0.0',
            pubspec: {},
            createdAt: DateTime.now().toIso8601String(),
            uploader: 'baz@bar.dev',
          ),
        );
        final packageJsonString =
            await File('$testFilePath/foo/meta/package.json').readAsString();
        final package = PubPackage.fromJson(json.decode(packageJsonString));

        expect(package.uploaders.length, 1);
        expect(package.uploaders.first, 'foo@bar.dev');
      });
    });

    group('queryPackage', () {
      test('directory exist but package does not', () async {
        final metaStore = MetaStoreFileSystem(testFilePath);

        final pubPackage = await metaStore.queryPackage('foo');

        expect(pubPackage, isNull);
      });

      test('directory exist but package does not', () async {
        final metaStore = MetaStoreFileSystem(testFilePath);

        final pubPackage = await metaStore.queryPackage('foo');

        expect(pubPackage, isNull);
      });

      test('directory does not exist', () async {
        final metaStore = MetaStoreFileSystem('./test/blah');

        final pubPackage = await metaStore.queryPackage('foo');

        expect(pubPackage, isNull);
      });
    });

    group('addUploader', () {
      tearDown(() async {
        await Directory(testFilePath).delete(recursive: true);
      });

      test('can add and remove uploader', () async {
        const expectedUploaderEmail = 'email@company.dev';
        final metaStore = MetaStoreFileSystem(testFilePath);
        await metaStore.addVersion(
          'foo',
          PubVersion(
            version: '0.0.0',
            pubspec: {},
            createdAt: DateTime.now().toIso8601String(),
            uploader: 'foo@bar.dev',
          ),
        );

        // add
        await metaStore.addUploader('foo', expectedUploaderEmail);
        var pubPackage = await metaStore.queryPackage('foo');

        expect(pubPackage!.uploaders.contains(expectedUploaderEmail), isTrue);

        // remove
        await metaStore.removeUploader('foo', expectedUploaderEmail);
        pubPackage = await metaStore.queryPackage('foo');

        expect(pubPackage!.uploaders.contains(expectedUploaderEmail), isFalse);
      });
    });
  });
}
