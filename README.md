# mcpub

- A headless pub server
- [Pub Spec V2](https://github.com/dart-lang/pub/blob/master/doc/repository-spec-v2.md) compliant
- Inspired and shamelessly cherry-picked from [unpub](https://github.com/bytedance/unpub)

## Docker Environment Vars

|       VAR        | Default | Results In                                                   |
|:----------------:|:-------:|--------------------------------------------------------------|
|       HOST       | 0.0.0.0 | address the server binds to                                  |
|       PORT       |  4040   | server listens on port 4040                                  |
| FILE_SYSTEM_PATH | /mcpub  | location of saved files if using  `*StoreFileSystem` Impl(s) |
|  WELL_KNOWN_URL  |         | uses OAuthProviderWellKnown instead of OAuthProviderNone     |

## Default Implementations

|     Impl      |        Default         | Available (PRs Welcome)                   |
|:-------------:|:----------------------:|-------------------------------------------|
| OAuthProvider |   OAuthProviderNone    | OAuthProviderNone, OAuthProviderWellKnown |
| PackageStore  | PackageStoreFileSystem | PackageStoreFileSystem                    |
|   MetaStore   |  MetaStoreFileSystem   | MetaStoreFileSystem                       |

## dev - PR's Welcome

- Autogen Models `dart run build_runner build --delete-conflicting-outputs`
