import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:jose/jose.dart';
import 'package:logging/logging.dart';
import 'package:mcpub/mcpub.dart';
import 'package:shelf/shelf.dart';

class OAuthProviderWellKnown extends OAuthProvider {
  OAuthProviderWellKnown({
    required String wellKnownUrl,
    http.Client? client,
    JsonWebKeyStore? keyStore,
  }) {
    _keyStore = keyStore ?? JsonWebKeyStore();
    _client = client ?? http.Client();
    _addKeys(wellKnownUrl);
  }

  late final http.Client _client;
  late final JsonWebKeyStore _keyStore;

  Future<void> _addKeys(String wellKnownUrl) async {
    final wellKnownResult = await _client.get(Uri.parse(wellKnownUrl));
    if (wellKnownResult.statusCode != 200 || wellKnownResult.body.isEmpty) {
      throw Exception('Failed or invalid wellKnownUrl response.');
    }
    final wellKnownJson = json.decode(wellKnownResult.body);
    final jwksUri = wellKnownJson['jwks_uri'];
    if (jwksUri == null || jwksUri is String && jwksUri.isEmpty) {
      throw Exception('well-known missing "jwks_uri" key in response.');
    }
    _keyStore.addKeySetUrl(Uri.parse(jwksUri));
    Logger.root.info('Successfully loaded JWKS keys. ✅');
  }

  JsonWebToken? _decodeJwtFromHeaders(Map<String, String> httpHeaders) {
    final authHeader = httpHeaders[HttpHeaders.authorizationHeader];
    if (authHeader != null && authHeader.isNotEmpty) {
      final authHeaderParts = authHeader.split(' ');
      return JsonWebToken.unverified(authHeaderParts.last);
    }
    return null;
  }

  @override
  Future<String> getPublisherEmail(Map<String, String> httpHeaders) async {
    final decodedJwt = _decodeJwtFromHeaders(httpHeaders);
    if (decodedJwt == null) {
      throw Exception('Missing JWT. Can not parse email.');
    }
    final subject = decodedJwt.claims.subject;
    final host = decodedJwt.claims.issuer?.host;
    if (subject != null && host != null) {
      return '$subject@$host';
    }

    throw Exception(
      'JWT missing a subject ("sub"). A subject is required when using the default OAuthProviderWellKnown.',
    );
  }

  @override
  Future<Response?> shouldAllowRequest(Request req) async {
    if (req.requestedUri.toString().contains('/healthz')) {
      // Always allow health check
      return null;
    }

    final decodedJwt = _decodeJwtFromHeaders(req.headers);
    if (decodedJwt != null) {
      if (await decodedJwt.verify(_keyStore)) {
        return null;
      }
    }

    return OAuthProvider.unauthorized('Unauthorized');
  }
}
