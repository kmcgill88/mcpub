// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PubVersion _$PubVersionFromJson(Map<String, dynamic> json) {
  return _PubVersion.fromJson(json);
}

/// @nodoc
mixin _$PubVersion {
  String get version => throw _privateConstructorUsedError;
  Map<String, dynamic> get pubspec => throw _privateConstructorUsedError;
  String get createdAt => throw _privateConstructorUsedError;
  String get uploader => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PubVersionCopyWith<PubVersion> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PubVersionCopyWith<$Res> {
  factory $PubVersionCopyWith(
          PubVersion value, $Res Function(PubVersion) then) =
      _$PubVersionCopyWithImpl<$Res, PubVersion>;
  @useResult
  $Res call(
      {String version,
      Map<String, dynamic> pubspec,
      String createdAt,
      String uploader});
}

/// @nodoc
class _$PubVersionCopyWithImpl<$Res, $Val extends PubVersion>
    implements $PubVersionCopyWith<$Res> {
  _$PubVersionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? version = null,
    Object? pubspec = null,
    Object? createdAt = null,
    Object? uploader = null,
  }) {
    return _then(_value.copyWith(
      version: null == version
          ? _value.version
          : version // ignore: cast_nullable_to_non_nullable
              as String,
      pubspec: null == pubspec
          ? _value.pubspec
          : pubspec // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      uploader: null == uploader
          ? _value.uploader
          : uploader // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PubVersionCopyWith<$Res>
    implements $PubVersionCopyWith<$Res> {
  factory _$$_PubVersionCopyWith(
          _$_PubVersion value, $Res Function(_$_PubVersion) then) =
      __$$_PubVersionCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String version,
      Map<String, dynamic> pubspec,
      String createdAt,
      String uploader});
}

/// @nodoc
class __$$_PubVersionCopyWithImpl<$Res>
    extends _$PubVersionCopyWithImpl<$Res, _$_PubVersion>
    implements _$$_PubVersionCopyWith<$Res> {
  __$$_PubVersionCopyWithImpl(
      _$_PubVersion _value, $Res Function(_$_PubVersion) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? version = null,
    Object? pubspec = null,
    Object? createdAt = null,
    Object? uploader = null,
  }) {
    return _then(_$_PubVersion(
      version: null == version
          ? _value.version
          : version // ignore: cast_nullable_to_non_nullable
              as String,
      pubspec: null == pubspec
          ? _value._pubspec
          : pubspec // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      uploader: null == uploader
          ? _value.uploader
          : uploader // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PubVersion implements _PubVersion {
  _$_PubVersion(
      {required this.version,
      required final Map<String, dynamic> pubspec,
      required this.createdAt,
      required this.uploader})
      : _pubspec = pubspec;

  factory _$_PubVersion.fromJson(Map<String, dynamic> json) =>
      _$$_PubVersionFromJson(json);

  @override
  final String version;
  final Map<String, dynamic> _pubspec;
  @override
  Map<String, dynamic> get pubspec {
    if (_pubspec is EqualUnmodifiableMapView) return _pubspec;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_pubspec);
  }

  @override
  final String createdAt;
  @override
  final String uploader;

  @override
  String toString() {
    return 'PubVersion(version: $version, pubspec: $pubspec, createdAt: $createdAt, uploader: $uploader)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PubVersion &&
            (identical(other.version, version) || other.version == version) &&
            const DeepCollectionEquality().equals(other._pubspec, _pubspec) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.uploader, uploader) ||
                other.uploader == uploader));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, version,
      const DeepCollectionEquality().hash(_pubspec), createdAt, uploader);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PubVersionCopyWith<_$_PubVersion> get copyWith =>
      __$$_PubVersionCopyWithImpl<_$_PubVersion>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PubVersionToJson(
      this,
    );
  }
}

abstract class _PubVersion implements PubVersion {
  factory _PubVersion(
      {required final String version,
      required final Map<String, dynamic> pubspec,
      required final String createdAt,
      required final String uploader}) = _$_PubVersion;

  factory _PubVersion.fromJson(Map<String, dynamic> json) =
      _$_PubVersion.fromJson;

  @override
  String get version;
  @override
  Map<String, dynamic> get pubspec;
  @override
  String get createdAt;
  @override
  String get uploader;
  @override
  @JsonKey(ignore: true)
  _$$_PubVersionCopyWith<_$_PubVersion> get copyWith =>
      throw _privateConstructorUsedError;
}

PubPackage _$PubPackageFromJson(Map<String, dynamic> json) {
  return _PubPackage.fromJson(json);
}

/// @nodoc
mixin _$PubPackage {
  String get name => throw _privateConstructorUsedError;
  List<PubVersion> get versions => throw _privateConstructorUsedError;
  String get createdAt => throw _privateConstructorUsedError;
  String get updatedAt => throw _privateConstructorUsedError;
  List<String> get uploaders => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PubPackageCopyWith<PubPackage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PubPackageCopyWith<$Res> {
  factory $PubPackageCopyWith(
          PubPackage value, $Res Function(PubPackage) then) =
      _$PubPackageCopyWithImpl<$Res, PubPackage>;
  @useResult
  $Res call(
      {String name,
      List<PubVersion> versions,
      String createdAt,
      String updatedAt,
      List<String> uploaders});
}

/// @nodoc
class _$PubPackageCopyWithImpl<$Res, $Val extends PubPackage>
    implements $PubPackageCopyWith<$Res> {
  _$PubPackageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? versions = null,
    Object? createdAt = null,
    Object? updatedAt = null,
    Object? uploaders = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      versions: null == versions
          ? _value.versions
          : versions // ignore: cast_nullable_to_non_nullable
              as List<PubVersion>,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      updatedAt: null == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String,
      uploaders: null == uploaders
          ? _value.uploaders
          : uploaders // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PubPackageCopyWith<$Res>
    implements $PubPackageCopyWith<$Res> {
  factory _$$_PubPackageCopyWith(
          _$_PubPackage value, $Res Function(_$_PubPackage) then) =
      __$$_PubPackageCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      List<PubVersion> versions,
      String createdAt,
      String updatedAt,
      List<String> uploaders});
}

/// @nodoc
class __$$_PubPackageCopyWithImpl<$Res>
    extends _$PubPackageCopyWithImpl<$Res, _$_PubPackage>
    implements _$$_PubPackageCopyWith<$Res> {
  __$$_PubPackageCopyWithImpl(
      _$_PubPackage _value, $Res Function(_$_PubPackage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? versions = null,
    Object? createdAt = null,
    Object? updatedAt = null,
    Object? uploaders = null,
  }) {
    return _then(_$_PubPackage(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      versions: null == versions
          ? _value._versions
          : versions // ignore: cast_nullable_to_non_nullable
              as List<PubVersion>,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      updatedAt: null == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String,
      uploaders: null == uploaders
          ? _value._uploaders
          : uploaders // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PubPackage implements _PubPackage {
  _$_PubPackage(
      {required this.name,
      required final List<PubVersion> versions,
      required this.createdAt,
      required this.updatedAt,
      required final List<String> uploaders})
      : _versions = versions,
        _uploaders = uploaders;

  factory _$_PubPackage.fromJson(Map<String, dynamic> json) =>
      _$$_PubPackageFromJson(json);

  @override
  final String name;
  final List<PubVersion> _versions;
  @override
  List<PubVersion> get versions {
    if (_versions is EqualUnmodifiableListView) return _versions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_versions);
  }

  @override
  final String createdAt;
  @override
  final String updatedAt;
  final List<String> _uploaders;
  @override
  List<String> get uploaders {
    if (_uploaders is EqualUnmodifiableListView) return _uploaders;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_uploaders);
  }

  @override
  String toString() {
    return 'PubPackage(name: $name, versions: $versions, createdAt: $createdAt, updatedAt: $updatedAt, uploaders: $uploaders)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PubPackage &&
            (identical(other.name, name) || other.name == name) &&
            const DeepCollectionEquality().equals(other._versions, _versions) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            const DeepCollectionEquality()
                .equals(other._uploaders, _uploaders));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      const DeepCollectionEquality().hash(_versions),
      createdAt,
      updatedAt,
      const DeepCollectionEquality().hash(_uploaders));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PubPackageCopyWith<_$_PubPackage> get copyWith =>
      __$$_PubPackageCopyWithImpl<_$_PubPackage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PubPackageToJson(
      this,
    );
  }
}

abstract class _PubPackage implements PubPackage {
  factory _PubPackage(
      {required final String name,
      required final List<PubVersion> versions,
      required final String createdAt,
      required final String updatedAt,
      required final List<String> uploaders}) = _$_PubPackage;

  factory _PubPackage.fromJson(Map<String, dynamic> json) =
      _$_PubPackage.fromJson;

  @override
  String get name;
  @override
  List<PubVersion> get versions;
  @override
  String get createdAt;
  @override
  String get updatedAt;
  @override
  List<String> get uploaders;
  @override
  @JsonKey(ignore: true)
  _$$_PubPackageCopyWith<_$_PubPackage> get copyWith =>
      throw _privateConstructorUsedError;
}

ListAllPubPackageVersions _$ListAllPubPackageVersionsFromJson(
    Map<String, dynamic> json) {
  return _ListAllPubPackageVersions.fromJson(json);
}

/// @nodoc
mixin _$ListAllPubPackageVersions {
  String get name => throw _privateConstructorUsedError;
  List<PubVersionWithArchiveUrl> get versions =>
      throw _privateConstructorUsedError;
  PubVersionWithArchiveUrl get latest => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListAllPubPackageVersionsCopyWith<ListAllPubPackageVersions> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListAllPubPackageVersionsCopyWith<$Res> {
  factory $ListAllPubPackageVersionsCopyWith(ListAllPubPackageVersions value,
          $Res Function(ListAllPubPackageVersions) then) =
      _$ListAllPubPackageVersionsCopyWithImpl<$Res, ListAllPubPackageVersions>;
  @useResult
  $Res call(
      {String name,
      List<PubVersionWithArchiveUrl> versions,
      PubVersionWithArchiveUrl latest});
}

/// @nodoc
class _$ListAllPubPackageVersionsCopyWithImpl<$Res,
        $Val extends ListAllPubPackageVersions>
    implements $ListAllPubPackageVersionsCopyWith<$Res> {
  _$ListAllPubPackageVersionsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? versions = null,
    Object? latest = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      versions: null == versions
          ? _value.versions
          : versions // ignore: cast_nullable_to_non_nullable
              as List<PubVersionWithArchiveUrl>,
      latest: null == latest
          ? _value.latest
          : latest // ignore: cast_nullable_to_non_nullable
              as PubVersionWithArchiveUrl,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ListAllPubPackageVersionsCopyWith<$Res>
    implements $ListAllPubPackageVersionsCopyWith<$Res> {
  factory _$$_ListAllPubPackageVersionsCopyWith(
          _$_ListAllPubPackageVersions value,
          $Res Function(_$_ListAllPubPackageVersions) then) =
      __$$_ListAllPubPackageVersionsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      List<PubVersionWithArchiveUrl> versions,
      PubVersionWithArchiveUrl latest});
}

/// @nodoc
class __$$_ListAllPubPackageVersionsCopyWithImpl<$Res>
    extends _$ListAllPubPackageVersionsCopyWithImpl<$Res,
        _$_ListAllPubPackageVersions>
    implements _$$_ListAllPubPackageVersionsCopyWith<$Res> {
  __$$_ListAllPubPackageVersionsCopyWithImpl(
      _$_ListAllPubPackageVersions _value,
      $Res Function(_$_ListAllPubPackageVersions) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? versions = null,
    Object? latest = null,
  }) {
    return _then(_$_ListAllPubPackageVersions(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      versions: null == versions
          ? _value._versions
          : versions // ignore: cast_nullable_to_non_nullable
              as List<PubVersionWithArchiveUrl>,
      latest: null == latest
          ? _value.latest
          : latest // ignore: cast_nullable_to_non_nullable
              as PubVersionWithArchiveUrl,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListAllPubPackageVersions implements _ListAllPubPackageVersions {
  _$_ListAllPubPackageVersions(
      {required this.name,
      required final List<PubVersionWithArchiveUrl> versions,
      required this.latest})
      : _versions = versions;

  factory _$_ListAllPubPackageVersions.fromJson(Map<String, dynamic> json) =>
      _$$_ListAllPubPackageVersionsFromJson(json);

  @override
  final String name;
  final List<PubVersionWithArchiveUrl> _versions;
  @override
  List<PubVersionWithArchiveUrl> get versions {
    if (_versions is EqualUnmodifiableListView) return _versions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_versions);
  }

  @override
  final PubVersionWithArchiveUrl latest;

  @override
  String toString() {
    return 'ListAllPubPackageVersions(name: $name, versions: $versions, latest: $latest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ListAllPubPackageVersions &&
            (identical(other.name, name) || other.name == name) &&
            const DeepCollectionEquality().equals(other._versions, _versions) &&
            (identical(other.latest, latest) || other.latest == latest));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name,
      const DeepCollectionEquality().hash(_versions), latest);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ListAllPubPackageVersionsCopyWith<_$_ListAllPubPackageVersions>
      get copyWith => __$$_ListAllPubPackageVersionsCopyWithImpl<
          _$_ListAllPubPackageVersions>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListAllPubPackageVersionsToJson(
      this,
    );
  }
}

abstract class _ListAllPubPackageVersions implements ListAllPubPackageVersions {
  factory _ListAllPubPackageVersions(
          {required final String name,
          required final List<PubVersionWithArchiveUrl> versions,
          required final PubVersionWithArchiveUrl latest}) =
      _$_ListAllPubPackageVersions;

  factory _ListAllPubPackageVersions.fromJson(Map<String, dynamic> json) =
      _$_ListAllPubPackageVersions.fromJson;

  @override
  String get name;
  @override
  List<PubVersionWithArchiveUrl> get versions;
  @override
  PubVersionWithArchiveUrl get latest;
  @override
  @JsonKey(ignore: true)
  _$$_ListAllPubPackageVersionsCopyWith<_$_ListAllPubPackageVersions>
      get copyWith => throw _privateConstructorUsedError;
}

AddPublisherPostBody _$AddPublisherPostBodyFromJson(Map<String, dynamic> json) {
  return _AddPublisherPostBody.fromJson(json);
}

/// @nodoc
mixin _$AddPublisherPostBody {
  String get uploaderEmail => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AddPublisherPostBodyCopyWith<AddPublisherPostBody> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddPublisherPostBodyCopyWith<$Res> {
  factory $AddPublisherPostBodyCopyWith(AddPublisherPostBody value,
          $Res Function(AddPublisherPostBody) then) =
      _$AddPublisherPostBodyCopyWithImpl<$Res, AddPublisherPostBody>;
  @useResult
  $Res call({String uploaderEmail});
}

/// @nodoc
class _$AddPublisherPostBodyCopyWithImpl<$Res,
        $Val extends AddPublisherPostBody>
    implements $AddPublisherPostBodyCopyWith<$Res> {
  _$AddPublisherPostBodyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uploaderEmail = null,
  }) {
    return _then(_value.copyWith(
      uploaderEmail: null == uploaderEmail
          ? _value.uploaderEmail
          : uploaderEmail // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AddPublisherPostBodyCopyWith<$Res>
    implements $AddPublisherPostBodyCopyWith<$Res> {
  factory _$$_AddPublisherPostBodyCopyWith(_$_AddPublisherPostBody value,
          $Res Function(_$_AddPublisherPostBody) then) =
      __$$_AddPublisherPostBodyCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String uploaderEmail});
}

/// @nodoc
class __$$_AddPublisherPostBodyCopyWithImpl<$Res>
    extends _$AddPublisherPostBodyCopyWithImpl<$Res, _$_AddPublisherPostBody>
    implements _$$_AddPublisherPostBodyCopyWith<$Res> {
  __$$_AddPublisherPostBodyCopyWithImpl(_$_AddPublisherPostBody _value,
      $Res Function(_$_AddPublisherPostBody) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uploaderEmail = null,
  }) {
    return _then(_$_AddPublisherPostBody(
      uploaderEmail: null == uploaderEmail
          ? _value.uploaderEmail
          : uploaderEmail // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AddPublisherPostBody implements _AddPublisherPostBody {
  _$_AddPublisherPostBody({required this.uploaderEmail});

  factory _$_AddPublisherPostBody.fromJson(Map<String, dynamic> json) =>
      _$$_AddPublisherPostBodyFromJson(json);

  @override
  final String uploaderEmail;

  @override
  String toString() {
    return 'AddPublisherPostBody(uploaderEmail: $uploaderEmail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddPublisherPostBody &&
            (identical(other.uploaderEmail, uploaderEmail) ||
                other.uploaderEmail == uploaderEmail));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, uploaderEmail);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AddPublisherPostBodyCopyWith<_$_AddPublisherPostBody> get copyWith =>
      __$$_AddPublisherPostBodyCopyWithImpl<_$_AddPublisherPostBody>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AddPublisherPostBodyToJson(
      this,
    );
  }
}

abstract class _AddPublisherPostBody implements AddPublisherPostBody {
  factory _AddPublisherPostBody({required final String uploaderEmail}) =
      _$_AddPublisherPostBody;

  factory _AddPublisherPostBody.fromJson(Map<String, dynamic> json) =
      _$_AddPublisherPostBody.fromJson;

  @override
  String get uploaderEmail;
  @override
  @JsonKey(ignore: true)
  _$$_AddPublisherPostBodyCopyWith<_$_AddPublisherPostBody> get copyWith =>
      throw _privateConstructorUsedError;
}
