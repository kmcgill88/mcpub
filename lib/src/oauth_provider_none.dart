import 'package:mcpub/mcpub.dart';
import 'package:shelf/shelf.dart';

///
/// An OAuth provider implementation that allows all actions
///
/// For Demo Purposes only
///
/// Typically you would decode a JWT in the header and return the users
/// email from there.
///
class OAuthProviderNone extends OAuthProvider {
  @override
  Future<String> getPublisherEmail(Map<String, String> httpHeaders) async {
    return 'demo@example.dev';
  }

  ///
  /// Returning null for a requests allows the request
  ///
  /// Conditionally block requests by returning a different shelf Response
  /// such as a 401
  ///
  @override
  Future<Response?> shouldAllowRequest(Request req) async {
    return null;
  }
}
