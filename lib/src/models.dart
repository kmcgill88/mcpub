import 'package:freezed_annotation/freezed_annotation.dart';

part 'models.freezed.dart';
part 'models.g.dart';

@freezed
class PubVersion with _$PubVersion {
  factory PubVersion({
    required String version,
    required Map<String, dynamic> pubspec,
    required String createdAt,
    required String uploader,
  }) = _PubVersion;

  factory PubVersion.fromJson(Map<String, dynamic> map) =>
      _$PubVersionFromJson(map);
}

extension PubVersionX on PubVersion {
  PubVersionWithArchiveUrl addArchiveUrl(String url) =>
      PubVersionWithArchiveUrl(
        version: version,
        pubspec: pubspec,
        createdAt: createdAt,
        uploader: uploader,
        archiveUrl: url,
      );
}

@JsonSerializable()
class PubVersionWithArchiveUrl {
  factory PubVersionWithArchiveUrl.fromJson(Map<String, dynamic> map) =>
      _$PubVersionWithArchiveUrlFromJson(map);

  PubVersionWithArchiveUrl({
    required this.version,
    required this.pubspec,
    required this.createdAt,
    required this.uploader,
    required this.archiveUrl,
  });

  @JsonKey(name: 'archive_url')
  final String archiveUrl;
  final String version;
  final Map<String, dynamic> pubspec;
  final String createdAt;
  final String uploader;

  Map<String, dynamic> toJson() => _$PubVersionWithArchiveUrlToJson(this);
}

@freezed
class PubPackage with _$PubPackage {
  factory PubPackage({
    required String name,
    required List<PubVersion> versions,
    required String createdAt,
    required String updatedAt,
    required List<String> uploaders,
  }) = _PubPackage;

  factory PubPackage.fromJson(Map<String, dynamic> map) =>
      _$PubPackageFromJson(map);
}

extension PubPackageX on PubPackage {
  PubVersion get latestVersion {
    if (versions.isEmpty) {
      throw Exception('PubPackage "$name" has no versions!');
    }

    PubVersion latest = versions.first;
    for (var element in versions) {
      if (DateTime.parse(element.createdAt)
          .isAfter(DateTime.parse(latest.createdAt))) {
        latest = element;
      }
    }

    return latest;
  }
}

@freezed
class ListAllPubPackageVersions with _$ListAllPubPackageVersions {
  factory ListAllPubPackageVersions({
    required String name,
    required List<PubVersionWithArchiveUrl> versions,
    required PubVersionWithArchiveUrl latest,
  }) = _ListAllPubPackageVersions;

  factory ListAllPubPackageVersions.fromJson(Map<String, dynamic> map) =>
      _$ListAllPubPackageVersionsFromJson(map);
}

@freezed
class AddPublisherPostBody with _$AddPublisherPostBody {
  factory AddPublisherPostBody({
    required String uploaderEmail,
  }) = _AddPublisherPostBody;

  factory AddPublisherPostBody.fromJson(Map<String, dynamic> map) =>
      _$AddPublisherPostBodyFromJson(map);
}
