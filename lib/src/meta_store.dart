import 'package:mcpub/mcpub.dart';

abstract class MetaStore {
  Future<PubPackage?> queryPackage(String packageName);

  Future<void> addVersion(String packageName, PubVersion version);

  Future<void> addUploader(String packageName, String email);

  Future<void> removeUploader(String packageName, String email);
}
