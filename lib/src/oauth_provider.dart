import 'package:shelf/shelf.dart';

abstract class OAuthProvider {
  Future<String> getPublisherEmail(Map<String, String> httpHeaders);

  /// Whether or not to allow the calling request.
  ///
  /// Return null to allow request
  /// or
  /// return OAuthProvider.unauthorized('i have no idea who you are');
  /// return OAuthProvider.forbidden('you dont have permission');
  ///
  /// https://github.com/dart-lang/pub/blob/master/doc/repository-spec-v2.md#missing-authentication-or-invalid-token
  Future<Response?> shouldAllowRequest(Request req);

  static Response unauthorized(String messageForPubClient) {
    return Response(
      401,
      headers: {
        'WWW-Authenticate':
            'Bearer realm="pub", message="$messageForPubClient"',
      },
    );
  }

  static Response forbidden(String messageForPubClient) {
    return Response(
      403,
      headers: {
        'WWW-Authenticate':
            'Bearer realm="pub", message="$messageForPubClient"',
      },
    );
  }
}
