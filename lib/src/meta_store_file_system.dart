import 'dart:convert';
import 'dart:io';

import 'package:mcpub/mcpub.dart';

class MetaStoreFileSystem extends MetaStore {
  MetaStoreFileSystem(this.baseDir);
  String baseDir;

  File _getVersionMetaFile(String packageName, String version) =>
      File('$baseDir/$packageName/meta/versions/$version.json');

  File _getPubPackageMetaFile(String packageName) =>
      File('$baseDir/$packageName/meta/package.json');

  Future<PubPackage> _upsertPackage(String packageName) async {
    final pubPackageFile = _getPubPackageMetaFile(packageName);
    if (!await pubPackageFile.exists()) {
      await _writePackage(
        PubPackage(
          name: packageName,
          createdAt: DateTime.now().toUtc().toIso8601String(),
          updatedAt: DateTime.now().toUtc().toIso8601String(),
          versions: [],
          uploaders: [],
        ),
      );
    }
    final fileAsString = await pubPackageFile.readAsString();
    final pubPackageJson = await json.decode(fileAsString);
    return PubPackage.fromJson(pubPackageJson);
  }

  Future<void> _writePackage(PubPackage package) async {
    final pubPackageFile = _getPubPackageMetaFile(package.name);
    await pubPackageFile.create(recursive: true);
    pubPackageFile.writeAsString(json.encode(package.toJson()));
  }

  @override
  Future<void> addVersion(String packageName, PubVersion version) async {
    final isFirstUpload = !await _getPubPackageMetaFile(packageName).exists();
    final pubPackage = await _upsertPackage(packageName);
    final versionFile = _getVersionMetaFile(packageName, version.version);
    await versionFile.create(recursive: true);
    await Future.wait([
      versionFile.writeAsString(json.encode(version.toJson())),
      _writePackage(
        pubPackage.copyWith(
          updatedAt: DateTime.now().toUtc().toIso8601String(),
          uploaders: isFirstUpload ? [version.uploader] : pubPackage.uploaders,
        ),
      ),
    ]);
  }

  @override
  Future<PubPackage?> queryPackage(String packageName) async {
    final versionsDir = Directory('$baseDir/$packageName/meta/versions');
    final pubPackageFile = _getPubPackageMetaFile(packageName);
    if (await versionsDir.exists() && await pubPackageFile.exists()) {
      final pubPackage = await _upsertPackage(packageName);
      final List<FileSystemEntity> entities = await versionsDir.list().toList();

      final pubVersions = entities.map((version) {
        final f = File(version.absolute.path);
        return json.decode(f.readAsStringSync());
      }).toList();

      return PubPackage.fromJson({
        ...pubPackage.toJson(),
        'versions': pubVersions,
      });
    }
    return null;
  }

  @override
  Future<void> addUploader(String packageName, String email) async {
    final pubPackage = await _upsertPackage(packageName);
    await _writePackage(
      pubPackage.copyWith(
        uploaders: [
          ...pubPackage.uploaders,
          email,
        ],
      ),
    );
  }

  @override
  Future<void> removeUploader(String packageName, String email) async {
    final pubPackage = await _upsertPackage(packageName);
    await _writePackage(
      pubPackage.copyWith(
        uploaders: [
          ...pubPackage.uploaders.where(
            (uploaderEmail) => uploaderEmail != email,
          ),
        ],
      ),
    );
  }
}
