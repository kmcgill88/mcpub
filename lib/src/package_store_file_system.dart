import 'dart:io';

import 'package:mcpub/mcpub.dart';

class PackageStoreFileSystem extends PackageStore {
  PackageStoreFileSystem(this.baseDir);
  String baseDir;

  File _getTarballFile(String packageName, String version) =>
      File('$baseDir/$packageName/archive/$version.tar.gz');

  @override
  Future<void> upload(String name, String version, List<int> content) async {
    var file = _getTarballFile(name, version);
    await file.create(recursive: true);
    await file.writeAsBytes(content);
  }

  @override
  Stream<List<int>> download(String name, String version) =>
      _getTarballFile(name, version).openRead();
}
