import 'dart:convert';
import 'dart:io';

import 'package:archive/archive.dart';
import 'package:http_parser/http_parser.dart';
import 'package:logging/logging.dart';
import 'package:mcpub/mcpub.dart';
import 'package:mime/mime.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_router/shelf_router.dart';
import 'package:yaml/yaml.dart';

class McPubServer {
  McPubServer({
    required this.metaStore,
    required this.packageStore,
    required this.oAuthProvider,
  });

  final Logger logger = Logger.root;

  /// meta information store
  final MetaStore metaStore;

  /// package(tarball) store
  final PackageStore packageStore;

  /// An OAuth2 Provider
  final OAuthProvider oAuthProvider;

  Future<HttpServer> serve({
    String host = '0.0.0.0',
    int port = 4040,
  }) async {
    final router = Router()
      ..get(
        '/healthz',
        _healthz,
      )
      ..get(
        '/api/packages/versions/new',
        _apiPackagesVersionsNew,
      )
      ..post(
        '/api/packages/versions/new/upload',
        _apiPackagesVersionsNewUpload,
      )
      ..get(
        '/api/packages/<packageName>',
        _apiPackagesListVersions,
      )
      ..get(
        '/packages/<packageName>/versions/<tarFileName>',
        _getBinary,
      )
      ..post(
        '/api/packages/<packageName>/uploader',
        _addUploader,
      )
      ..delete(
        '/api/packages/<packageName>/uploader/<uploaderEmail>',
        _removeUploader,
      )
      ..get(
        '/api/packages/<packageName>/finalize/<version>',
        _apiPackagesPackageFinalize,
      );

    var handler = const Pipeline()
        .addMiddleware(logRequests())
        .addMiddleware((Handler handler) {
      return (Request request) async {
        final shouldNotAllow = await oAuthProvider.shouldAllowRequest(request);
        if (shouldNotAllow != null) {
          return shouldNotAllow;
        }
        return handler(request);
      };
    }).addHandler(router);

    final server = await io.serve(handler, host, port);
    logger.info('🤘🏼Pub Server listening on $host:$port 🚀');
    return server;
  }

  Response _healthz(Request req) => Response.ok(
        json.encode(
          {
            'now': DateTime.now().toUtc().toIso8601String(),
          },
        ),
        headers: {
          HttpHeaders.contentTypeHeader: ContentType.json.mimeType,
        },
      );

  // https://github.com/dart-lang/pub/blob/master/doc/repository-spec-v2.md#publishing-packages
  // /api/packages/versions/new
  Response _apiPackagesVersionsNew(Request request) {
    final url = '${request.requestedUri.toString()}/upload';
    logger.info('New upload url requested: "$url"');
    return Response.ok(
      json.encode(
        {
          'url': url,
          'fields': {},
        },
      ),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
      },
    );
  }

  Response _apiPackagesPackageFinalize(
    Request request,
    String packageName,
    String version,
  ) {
    final message = '"$packageName@$version" uploaded successfully 🚀';
    logger.info(message);
    return Response.ok(
      json.encode({
        'success': {
          'message': message,
        },
      }),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
      },
    );
  }

  // https://github.com/dart-lang/pub/blob/master/doc/repository-spec-v2.md#list-all-versions-of-a-package
  Future<Response> _apiPackagesListVersions(
    Request request,
    String packageName,
  ) async {
    try {
      logger.info('Listing versions for package: "$packageName"');
      final pubPackage = await metaStore.queryPackage(packageName);
      if (pubPackage == null) {
        logger.info('"$packageName" was not found.');
        return Response.notFound(
          json.encode(
            {
              'error': {
                'message': '"$packageName" not found.',
              },
            },
          ),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }

      final latest = pubPackage.latestVersion;
      logger.info('Found latest package version: "$packageName@$latest"');
      return Response.ok(
        json.encode(
          ListAllPubPackageVersions(
            name: pubPackage.name,
            versions: pubPackage.versions
                .map(
                  (e) => e.addArchiveUrl(
                    buildArchiveUrl(
                      request.requestedUri,
                      packageName,
                      e.version,
                    ),
                  ),
                )
                .toList(),
            latest: latest.addArchiveUrl(
              buildArchiveUrl(
                request.requestedUri,
                packageName,
                latest.version,
              ),
            ),
          ).toJson(),
        ),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
        },
      );
    } catch (exception, stackTrace) {
      logger.shout('Failed to list package versions.');
      logger.shout('Exception details:\n $exception');
      logger.shout('Stack trace:\n $stackTrace');
      return Response(500);
    }
  }

  Future<Response> _getBinary(
    Request req,
    String packageName,
    String tarFileName,
  ) async {
    try {
      logger.info('Fetching package: "$packageName"');
      final version =
          Uri.decodeComponent(tarFileName).replaceAll('.tar.gz', '');

      final package = await metaStore.queryPackage(packageName);
      if (package == null) {
        logger.info('"$packageName" not found.');
        return Response.notFound(
          json.encode(
            {
              'error': {
                'message': '"$packageName" not found.',
              },
            },
          ),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }

      final packageVersion = package.versions.where(
        (item) => item.version == version,
      );
      if (packageVersion.isEmpty) {
        logger.info('"$packageName@$packageVersion" not found.');
        return Response.notFound(
          json.encode(
            {
              'error': {
                'message': '"$packageName@$version" not found.',
              },
            },
          ),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }

      logger.info('Returning binary for "$packageName@$packageVersion"');
      return Response.ok(
        packageStore.download(packageName, version),
        headers: {HttpHeaders.contentTypeHeader: ContentType.binary.mimeType},
      );
    } catch (exception, stackTrace) {
      logger.shout('Failed to get binary.');
      logger.shout('Exception details:\n $exception');
      logger.shout('Stack trace:\n $stackTrace');
      return Response(500);
    }
  }

  Future<Response> _apiPackagesVersionsNewUpload(Request request) async {
    try {
      final contentType = request.headers['content-type'];
      if (contentType == null) {
        logger.shout('Package upload missing "content-type header."');
        return Response.badRequest(
          headers: {HttpHeaders.contentTypeHeader: ContentType.json.mimeType},
          body: json.encode({
            'error': {'message': 'Missing "content-type header."'},
          }),
        );
      }

      final mediaType = MediaType.parse(contentType);
      final boundary = mediaType.parameters['boundary'];
      if (boundary == null) {
        logger.shout('Package upload invalid boundary.');
        return Response.badRequest(
          headers: {HttpHeaders.contentTypeHeader: ContentType.json.mimeType},
          body: json.encode({
            'error': {'message': 'Invalid boundary.'},
          }),
        );
      }

      final transformer = MimeMultipartTransformer(boundary);
      MimeMultipart? fileData;

      final stream = request.read().map((a) => a).transform(transformer);
      await for (var part in stream) {
        if (fileData != null) continue;
        fileData = part;
      }

      final bb = await fileData!.fold(
        // ignore: deprecated_export_use
        BytesBuilder(),
        (byteBuilder, d) => byteBuilder
          ..add(
            d,
          ),
      );
      final tarballBytes = bb.takeBytes();
      final tarBytes = GZipDecoder().decodeBytes(tarballBytes);
      final archive = TarDecoder().decodeBytes(tarBytes);
      ArchiveFile? pubspecArchiveFile;

      for (var file in archive.files) {
        if (file.name == 'pubspec.yaml') {
          pubspecArchiveFile = file;
          break;
        }
      }

      if (pubspecArchiveFile == null) {
        logger.shout('Missing pubspec.yaml file in upload.');
        return Response.badRequest(
          headers: {HttpHeaders.contentTypeHeader: ContentType.json.mimeType},
          body: json.encode({
            'error': {'message': 'Missing pubspec.yaml file in upload.'},
          }),
        );
      }

      final pubspecYaml = utf8.decode(pubspecArchiveFile.content);
      final pubspec = loadYamlAsMap(pubspecYaml)!;

      final name = pubspec['name'] as String;
      final version = pubspec['version'] as String;
      final package = await metaStore.queryPackage(name);

      final uploader = await oAuthProvider.getPublisherEmail(request.headers);
      // Package already exists
      if (package != null) {
        // Check uploaders
        if (!package.uploaders.contains(uploader)) {
          logger.shout(
            '"$uploader" is not an allowed uploader for package "$name"',
          );
          return Response.badRequest(
            headers: {HttpHeaders.contentTypeHeader: ContentType.json.mimeType},
            body: json.encode({
              'error': {
                'message':
                    '"$uploader" is not an allowed uploader for package "$name"',
              },
            }),
          );
        }

        // Check duplicated version
        final isDuplicated = package.versions
            .where((item) => version == item.version)
            .isNotEmpty;
        if (isDuplicated) {
          logger.shout('Invalid Version: "$name@$version" already exists.');
          return Response.badRequest(
            headers: {HttpHeaders.contentTypeHeader: ContentType.json.mimeType},
            body: json.encode({
              'error': {
                'message': 'Invalid Version: "$name@$version" already exists.',
              },
            }),
          );
        }
      }

      logger.info('Uploading "$name@$version".');
      await Future.wait([
        packageStore.upload(name, version, tarballBytes),
        metaStore.addVersion(
          name,
          PubVersion(
            version: version,
            pubspec: pubspec,
            createdAt: DateTime.now().toUtc().toIso8601String(),
            uploader: uploader,
          ),
        ),
      ]);
      logger.info('"$name@$version" uploaded successfully! 🚀');
      return Response(
        204,
        headers: {
          'Location': buildUploadFinalizeUrl(
            request.requestedUri,
            name,
            version,
          ),
        },
      );
    } catch (exception, stackTrace) {
      logger.shout('Failed to upload get binary.');
      logger.shout('Exception details:\n $exception');
      logger.shout('Stack trace:\n $stackTrace');
      return Response(500);
    }
  }

  Future<Response> _addUploader(Request req, String packageName) async {
    String? uploaderEmailToAdd;
    try {
      final body = await req.readAsString();
      uploaderEmailToAdd = AddPublisherPostBody.fromJson(
        json.decode(body),
      ).uploaderEmail;

      final operatorEmail = await oAuthProvider.getPublisherEmail(req.headers);
      final package = await metaStore.queryPackage(packageName);
      if (package == null) {
        logger.info('Add uploader; "$packageName" not found.');
        return Response.notFound(
          json.encode({
            'error': {'message': '"$packageName" not found.'},
          }),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }
      if (!package.uploaders.contains(operatorEmail)) {
        logger.info(
          '"$operatorEmail" is not authorized to add an uploader to "$package".',
        );
        return Response.forbidden(
          json.encode({
            'error': {
              'message':
                  '"$operatorEmail" is not authorized to add an uploader.',
            },
          }),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }
      if (package.uploaders.contains(uploaderEmailToAdd)) {
        logger.info(
          '"$uploaderEmailToAdd" already exists as an uploader for "$package".',
        );
        return Response.badRequest(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
          body: json.encode({
            'error': {
              'message': '"$uploaderEmailToAdd" already exists as an uploader.',
            },
          }),
        );
      }

      logger.info(
        'Adding "$uploaderEmailToAdd" as uploader for "$packageName".',
      );
      await metaStore.addUploader(packageName, uploaderEmailToAdd);
      logger.info(
        '"$uploaderEmailToAdd" added to "$packageName" successfully.',
      );
      return Response.ok(
        json.encode(
          {
            'success': {
              'message':
                  '"$uploaderEmailToAdd" added to "$packageName" successfully.',
            },
          },
        ),
        headers: {
          HttpHeaders.contentTypeHeader: ContentType.json.mimeType,
        },
      );
    } catch (exception, stackTrace) {
      logger.shout(
        'Failed to add $uploaderEmailToAdd added to "$packageName" successfully.',
      );
      logger.shout('Exception details:\n $exception');
      logger.shout('Stack trace:\n $stackTrace');
      return Response.badRequest(
        headers: {
          HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
        },
        body: json.encode({
          'error': {
            'message': exception.toString(),
            'hint':
                'Be sure to POST a body like {"uploaderEmail": "foo@bar.dev"}',
          },
        }),
      );
    }
  }

  Future<Response> _removeUploader(
    Request req,
    String packageName,
    String uploaderEmail,
  ) async {
    try {
      final operatorEmail = await oAuthProvider.getPublisherEmail(req.headers);
      final package = await metaStore.queryPackage(packageName);
      if (package == null) {
        logger.info('Remove uploader; "$packageName" not found.');
        return Response.notFound(
          json.encode({
            'error': {'message': '"$packageName" not found.'},
          }),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }
      if (!package.uploaders.contains(operatorEmail)) {
        logger.info(
          '"$operatorEmail" is not authorized to add an uploader to "$package".',
        );
        return Response.forbidden(
          json.encode({
            'error': {
              'message':
                  '"$operatorEmail" is not authorized to remove an uploader.',
            },
          }),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
        );
      }
      if (!package.uploaders.contains(uploaderEmail)) {
        logger.info('"$uploaderEmail" is not an uploader for "$packageName"');
        return Response.badRequest(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/vnd.pub.v2+json',
          },
          body: json.encode({
            'error': {
              'message':
                  '"$uploaderEmail" is not an uploader for "$packageName"',
            },
          }),
        );
      }

      logger.info('Removing "$uploaderEmail" from "$packageName"');
      await metaStore.removeUploader(packageName, uploaderEmail);
      logger.info('"$uploaderEmail" removed from "$packageName" successfully.');
      return Response.ok(
        json.encode(
          {
            'success': {
              'message':
                  '"$uploaderEmail" removed from "$packageName" successfully.',
            },
          },
        ),
        headers: {
          HttpHeaders.contentTypeHeader: ContentType.json.mimeType,
        },
      );
    } catch (exception, stackTrace) {
      logger.shout(
        'Failed to remove "$uploaderEmail" removed from "$packageName"',
      );
      logger.shout('Exception details:\n $exception');
      logger.shout('Stack trace:\n $stackTrace');
      return Response(500);
    }
  }
}

dynamic convertYaml(dynamic value) {
  if (value is YamlMap) {
    return value
        .cast<String, dynamic>()
        .map((k, v) => MapEntry(k, convertYaml(v)));
  }
  if (value is YamlList) {
    return value.map((e) => convertYaml(e)).toList();
  }
  return value;
}

Map<String, dynamic>? loadYamlAsMap(dynamic value) {
  var yamlMap = loadYaml(value) as YamlMap?;
  return convertYaml(yamlMap).cast<String, dynamic>();
}

String buildUploadFinalizeUrl(
  Uri uri,
  String packageName,
  String version,
) {
  return [
    uri.scheme,
    '://',
    uri.host,
    uri.hasPort ? ':${uri.port}' : '',
    '/api/packages/',
    packageName,
    '/finalize',
    '/',
    version,
  ].join();
}

String buildArchiveUrl(
  Uri uri,
  String packageName,
  String version,
) {
  return [
    uri.scheme,
    '://',
    uri.host,
    uri.hasPort ? ':${uri.port}' : '',
    '/packages/',
    packageName,
    '/versions/',
    version,
    '.tar.gz',
  ].join();
}
