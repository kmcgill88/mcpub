// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PubVersionWithArchiveUrl _$PubVersionWithArchiveUrlFromJson(
        Map<String, dynamic> json) =>
    PubVersionWithArchiveUrl(
      version: json['version'] as String,
      pubspec: json['pubspec'] as Map<String, dynamic>,
      createdAt: json['createdAt'] as String,
      uploader: json['uploader'] as String,
      archiveUrl: json['archive_url'] as String,
    );

Map<String, dynamic> _$PubVersionWithArchiveUrlToJson(
        PubVersionWithArchiveUrl instance) =>
    <String, dynamic>{
      'archive_url': instance.archiveUrl,
      'version': instance.version,
      'pubspec': instance.pubspec,
      'createdAt': instance.createdAt,
      'uploader': instance.uploader,
    };

_$_PubVersion _$$_PubVersionFromJson(Map<String, dynamic> json) =>
    _$_PubVersion(
      version: json['version'] as String,
      pubspec: json['pubspec'] as Map<String, dynamic>,
      createdAt: json['createdAt'] as String,
      uploader: json['uploader'] as String,
    );

Map<String, dynamic> _$$_PubVersionToJson(_$_PubVersion instance) =>
    <String, dynamic>{
      'version': instance.version,
      'pubspec': instance.pubspec,
      'createdAt': instance.createdAt,
      'uploader': instance.uploader,
    };

_$_PubPackage _$$_PubPackageFromJson(Map<String, dynamic> json) =>
    _$_PubPackage(
      name: json['name'] as String,
      versions: (json['versions'] as List<dynamic>)
          .map((e) => PubVersion.fromJson(e as Map<String, dynamic>))
          .toList(),
      createdAt: json['createdAt'] as String,
      updatedAt: json['updatedAt'] as String,
      uploaders:
          (json['uploaders'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_PubPackageToJson(_$_PubPackage instance) =>
    <String, dynamic>{
      'name': instance.name,
      'versions': instance.versions,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'uploaders': instance.uploaders,
    };

_$_ListAllPubPackageVersions _$$_ListAllPubPackageVersionsFromJson(
        Map<String, dynamic> json) =>
    _$_ListAllPubPackageVersions(
      name: json['name'] as String,
      versions: (json['versions'] as List<dynamic>)
          .map((e) =>
              PubVersionWithArchiveUrl.fromJson(e as Map<String, dynamic>))
          .toList(),
      latest: PubVersionWithArchiveUrl.fromJson(
          json['latest'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ListAllPubPackageVersionsToJson(
        _$_ListAllPubPackageVersions instance) =>
    <String, dynamic>{
      'name': instance.name,
      'versions': instance.versions,
      'latest': instance.latest,
    };

_$_AddPublisherPostBody _$$_AddPublisherPostBodyFromJson(
        Map<String, dynamic> json) =>
    _$_AddPublisherPostBody(
      uploaderEmail: json['uploaderEmail'] as String,
    );

Map<String, dynamic> _$$_AddPublisherPostBodyToJson(
        _$_AddPublisherPostBody instance) =>
    <String, dynamic>{
      'uploaderEmail': instance.uploaderEmail,
    };
