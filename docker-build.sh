#!/usr/bin/env sh
set -e

LATEST_GIT_TAG=$(git describe --abbrev=0 --tags)

echo "$CI_REGISTRY_USER"
echo "$CI_REGISTRY"
echo "$CI_REGISTRY_IMAGE"

echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$CI_REGISTRY" --password-stdin

docker build \
  -t registry.gitlab.com/kmcgill88/mcpub:latest \
  -t "registry.gitlab.com/kmcgill88/mcpub:${LATEST_GIT_TAG}" \
  .

docker push registry.gitlab.com/kmcgill88/mcpub:latest
docker push "registry.gitlab.com/kmcgill88/mcpub:${LATEST_GIT_TAG}"
